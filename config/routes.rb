Rails.application.routes.draw do
  resources :videos

  root to: 'visitors#index'
  get '/support', to: 'visitors#support'
  get '/stats', to: 'visitors#stats'

  devise_for :users
  resources :users
end
