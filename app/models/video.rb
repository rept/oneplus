class Video < ActiveRecord::Base

  validate :check_link
  belongs_to :user
  self.per_page = 10

  def frame
    %Q{<iframe title="YouTube video player" width="640" height="390" src="http://www.youtube.com/embed/#{ self.link }" frameborder="0" allowfullscreen></iframe>}
  end

  private

  def check_link
    if self.link[/youtu\.be\/([^\?]*)/]
      youtube_id = $1
    else
      # Regex from # http://stackoverflow.com/questions/3452546/javascript-regex-how-to-get-youtube-video-id-from-url/4811367#4811367
      self.link[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
      youtube_id = $5
    end

    if youtube_id.blank?
      errors.add(:link, "invalid youtube link")
    else
      self.link = youtube_id
    end
  end

end
