class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :description
      t.string :link
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
